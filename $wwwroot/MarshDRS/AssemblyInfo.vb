Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("MarshDRS")> 
<Assembly: AssemblyDescription("Marsh Document Receiver Service")> 
<Assembly: AssemblyCompany("Balboa Peaks")> 
<Assembly: AssemblyProduct("MarshDRS")> 
<Assembly: AssemblyCopyright("Copyright � 2012 Balboa Peaks Software")> 
<Assembly: AssemblyTrademark("All Rights Reserved")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("26714FF5-B179-4F9D-86DE-C8240A965613")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.0.*")> 