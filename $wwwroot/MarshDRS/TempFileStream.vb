Imports System.IO


Namespace DMSWebAPI


    Public Class TempFileStream
        Inherits IO.FileStream
        Private fullpath As String
        Sub New(ByVal filepath)
            MyBase.New(filepath, FileMode.Open, FileAccess.Read)
            fullpath = filepath
        End Sub
        ' Deletes our temp file when the filestream is released.
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            MyBase.Dispose(disposing)
            Try
                If (File.Exists(fullpath)) Then
                    File.Delete(fullpath)
                End If
            Catch ex As Exception
                TraceExceptions("TempFileStream.Dispose", ex)
            End Try
        End Sub

    End Class

End Namespace