Imports System
Imports System.Configuration


Namespace DMSWebAPI

    Public Class Config
        Shared DMS As DMSConfig
        Shared ConfigRead As Boolean = False
        Sub New()

            If (ConfigRead = False) Then
                Try
                    Dim config As New Collection

                    DMS.Server = ConfigurationSettings.AppSettings("DMSSERVER")
                    DMS.Domain = ConfigurationSettings.AppSettings("DMSDOMAIN")
                    DMS.User = ConfigurationSettings.AppSettings("DMSUSER")
                    DMS.Pass = ConfigurationSettings.AppSettings("DMSPASS")
                    DMS.TempDir = ConfigurationSettings.AppSettings("TEMPDIR")
                    DMS.CacheExpireInSeconds = Val(ConfigurationSettings.AppSettings("CACHETIMEOUT"))
                    ConfigRead = True

                Catch ex As Exception

                End Try

            End If

        End Sub
        Public Function GetTempDir() As String
            Return DMS.TempDir
        End Function
        Public Function GetServer() As String
            Return DMS.Server
        End Function
        Public Function GetDomain() As String
            Return DMS.Domain
        End Function
        Public Function GetUser() As String
            Return DMS.User
        End Function
        Public Function GetPass() As String
            Return DMS.Pass
        End Function
        Public Function GetCacheTimeout() As Long
            Return DMS.CacheExpireInSeconds
        End Function
    End Class

End Namespace
