Imports System.Runtime.InteropServices
Imports System.Reflection
Imports System.IO


Namespace DMSWebAPI



    Public Module Utility


        Sub DbgMsg(ByVal Msg As String)
            Dim NewMsg As String
            NewMsg = "DMSWebAPI: " + Msg
            Trace.WriteLine(NewMsg)


        End Sub
        Sub TraceExceptions(ByVal ProcName As String, ByVal ex As Exception)

            Dim NewMsg As String
            NewMsg = ProcName + ": The following exception occurred: " + ex.ToString()
            Trace.WriteLine("DMSWebAPI:" + NewMsg)
            'check the InnerException
            While Not (ex.InnerException Is Nothing)
                Trace.WriteLine("DMSWebAPI: " + ProcName + ": --------------------------------")
                Trace.WriteLine("DMSWebAPI: " + ProcName + ":  Next: " + ex.InnerException.ToString())
                ex = ex.InnerException
            End While

        End Sub
        Public Function GetAppInfo(ByVal ai As [Global].EAppInfo) As String

            Dim rc As String
            Select Case ai

                Case [Global].EAppInfo.aiCompany
                    Dim objCompany As AssemblyCompanyAttribute
                    objCompany = AssemblyCompanyAttribute.GetCustomAttribute(System.Reflection.Assembly.GetExecutingAssembly, GetType(AssemblyCompanyAttribute))
                    rc = objCompany.Company.ToString()

                Case [Global].EAppInfo.aiCopyright
                    Dim objCopyright As AssemblyCopyrightAttribute
                    objCopyright = AssemblyCopyrightAttribute.GetCustomAttribute(System.Reflection.Assembly.GetExecutingAssembly, GetType(AssemblyCopyrightAttribute))
                    rc = objCopyright.Copyright.ToString()

                Case [Global].EAppInfo.aiVersion
                    rc = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version.ToString()

            End Select

            Return rc

        End Function

    End Module

End Namespace
