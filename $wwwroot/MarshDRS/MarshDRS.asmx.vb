Imports System.Web.Services
Imports System.IO
Imports MarshDRS.DMSWebAPI
Imports Microsoft.Web.Services2
Imports Microsoft.Web.Services2.Dime


Namespace MarshDRS


    <System.Web.Services.WebService(Namespace:="http://tempuri.org/MarshDRS/DRS")> _
    Public Class DRS
        Inherits System.Web.Services.WebService
        'Shared Cfg As Config
        Shared oSession As VIPDMS.Session
        Shared DMS As DMSServer



#Region " Web Services Designer Generated Code "

        Public Sub New()

            MyBase.New()

            'This call is required by the Web Services Designer.
            InitializeComponent()
            oSession = Application.Get("DMSSession")
            DMS = Application.Get("DMSServer")

            'Delete all files in the temp directory
            'DeleteTempFiles()

        End Sub

        'Required by the Web Services Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Web Services Designer
        'It can be modified using the Web Services Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            components = New System.ComponentModel.Container
        End Sub

        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            'CODEGEN: This procedure is required by the Web Services Designer
            'Do not modify it using the code editor.
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

#End Region
        <System.Web.Services.WebMethod(Description:="Returns the VIPDMS Control's Version String")> _
        Public Function Version() As String
            Dim rc As String

            rc = DMS.Version()
            Return rc

        End Function
        <System.Web.Services.WebMethod(Description:="Returns MarshDRS Server Information - Valid InfoTypes are 0, 1 and 2.")> _
        Public Function AppInfo(ByVal InfoType As [Global].EAppInfo) As String
            Dim rc As String
            rc = "Invalid InfoType Specified"

            Select Case InfoType

                Case [Global].EAppInfo.aiVersion
                    rc = Microsoft.Web.Services2.Utility.GetAppInfo([Global].EAppInfo.aiVersion)
                Case [Global].EAppInfo.aiCopyright
                    rc = Microsoft.Web.Services2.Utility.GetAppInfo([Global].EAppInfo.aiCopyright)
                Case [Global].EAppInfo.aiCompany
                    rc = Microsoft.Web.Services2.Utility.GetAppInfo([Global].EAppInfo.aiCompany)
                Case Else
                    Throw New Exception("Invalid InfoType Specified")

            End Select
            Return rc
        End Function
        <System.Web.Services.WebMethod(Description:="Returns a string array containing the list of Document Classes defined on the server.")> _
        Public Function GetDocClassList() As String()

            Dim rc() As String
            Dim oSession As VIPDMS.Session

            rc = DMS.GetDocClasses()
            'HttpContext.Current.Session("DMS") = DMS
            Return rc
        End Function
        <System.Web.Services.WebMethod(Description:="Returns an array of FieldInfo structures describing the fields for the specified document class.")> _
        Public Function GetFieldInfo(ByVal DocClass As String) As [Global].FieldInfo()
            Dim rc() As [Global].FieldInfo
            DMS.Refresh()
            rc = DMS.GetFieldInfo(DocClass)
            Return rc
        End Function

        <System.Web.Services.WebMethod(Description:="Searches the specified Document Class using the specified criteria, and returns a SrchResults structure.")> _
        Public Function DoSearch(ByVal Search As [Global].SearchCriteria) As [Global].SrchResults

            Dim DSearch As New DMSServer.DMSSearch
            Dim rs As ADODB.Recordset
            Dim sr As [Global].SrchResults
            Dim rc As [Global].CacheOpStatus
            Dim ci As Long = 0

            DSearch = PrepareSearch(Search)
            rs = DMS.ExecuteSearch(DSearch)
            If (rs Is Nothing) Then
                DMS.RefreshSession()
                rs = DMS.ExecuteSearch(DSearch)
            End If

            If Not (rs Is Nothing) Then
                ' If Max hits = 0 then return all results
                If Search.MaxHits = 0 Then Search.MaxHits = rs.RecordCount
                If Search.CacheResults Then
                    'Context.Cache.Insert()
                    rc = [Global].GCache.AddRecordSet(rs, ci)
                End If
                sr = ParseSearchResults(rs, Search.MaxHits)
                sr.CacheID = ci

            Else
                sr.HitCount = 0
            End If

            Return sr
        End Function
        <System.Web.Services.WebMethod(Description:="Returns more results from the cache in a SrchResults structure.")> _
        Public Function GetCachedHits(ByVal ID As Long, ByVal StartRec As Long, ByVal Count As Long) As [Global].SrchResults

            Dim rs As ADODB.Recordset
            Dim sr As [Global].SrchResults
            Dim rc As [Global].CacheOpStatus

            If ([Global].GCache.GetRecordSet(ID, rs) = [Global].CacheOpStatus.cosOK) Then
                sr = ParseSearchResultsEx(rs, StartRec, Count)
                sr.Start = StartRec

            Else
                Throw New ArgumentException("Error retrieving RecordSet from Cache: " & [Global].GCache.GetLastExceptionMsg)
            End If

            Return sr
        End Function

        <System.Web.Services.WebMethod(Description:="Retrieves a given file using information in the specified ItemMetaData structure")> _
        Public Function RetrieveFile(ByVal Item As [Global].ItemMetaData) As String

            Dim TempFilePath As String
            TempFilePath = GetTempFileName(Item.FileType)
            If (DMS.GetImage(TempFilePath, Item)) Then
                Dim fs = New TempFileStream(TempFilePath)
                Dim infile As New IO.BinaryReader(fs)
                Dim attach As New DimeAttachment(Item.ContentType, TypeFormat.MediaType, infile.BaseStream)
                attach.ChunkSize = 8192
                ResponseSoapContext.Current.Attachments.Add(attach)
                Return Path.GetFileName(TempFilePath)
            Else
                Return ""
            End If

        End Function
        <System.Web.Services.WebMethod(Description:="Uploads a Document to the DMS, returning the Batch and Sequence Number as a comma seperated string.  Note: Field name/value pairs in the FieldValues() array are NOT position dependent.")> _
        Public Function UploadFile(ByVal DocClass As String, ByVal FieldValues() As [Global].DMSField) As String

            Dim Values() As String

            'Make sure precisely one attachment was sent
            If RequestSoapContext.Current.Attachments.Count <> 1 Then
                Throw New ArgumentException("Please upload one file at a time...")
            End If

            If (ValidateFields(DocClass, FieldValues, Values) <> True) Then
                Throw New ArgumentException("Invalid fields (or field lengths) specified")
            End If
            Dim ftmp As String

            ftmp = GetTempFileName(RequestSoapContext.Current.Attachments(0).ContentType)
            Dim fs As FileStream = File.Create(ftmp)
            Dim FileLength As Integer = RequestSoapContext.Current.Attachments(0).Stream.Length
            Dim buffer(FileLength) As Byte
            RequestSoapContext.Current.Attachments(0).Stream.Read(buffer, 0, FileLength)
            fs.Write(buffer, 0, FileLength)
            fs.Close()
            Dim BatchSeq As String
            BatchSeq = DMS.UploadFile(DocClass, Values, ftmp)
            File.Delete(ftmp)
            Return BatchSeq

        End Function
        <System.Web.Services.WebMethod(Description:="Adds an additional set of index values to document identified by DocID.  Note: Field name/value pairs in the FieldValues() array are NOT position dependent.")> _
        Public Function AddDocLink(ByVal ContentID As String, ByVal FieldValues() As [Global].DMSField, ByVal StartPage As Integer, ByVal EndPage As Integer, ByVal bImage As Boolean) As String

            Dim Names() As String
            Dim Values() As String
            Dim DocClass As String
            Dim rc As String

            If (ParseFieldData(FieldValues, Names, Values)) Then
                rc = DMS.AddDocLink(ContentID, Names, Values, bImage, StartPage, EndPage)
            Else
                Throw New ArgumentException("Invalid fields (or field lengths) specified")
            End If

            Return rc

        End Function
        <System.Web.Services.WebMethod(Description:="Returns an IndexID built from the provided data.")> _
        Public Function BuildIndexID(ByVal DocClass As String, ByVal BatchNumber As String, ByVal Sequence As String, ByVal doclinksequence As String) As String
            Dim rc As String
            rc = DMS.BuildIndexID(DocClass, BatchNumber, Sequence, doclinksequence)
            Return rc
        End Function
        <System.Web.Services.WebMethod(Description:="Returns an ContentID built from the provided data.")> _
        Public Function BuildContentID(ByVal DocClass As String, ByVal BatchNumber As String, ByVal Sequence As String) As String
            Dim rc As String
            rc = DMS.BuildContentID(DocClass, BatchNumber, Sequence)
            Return rc
        End Function

        <System.Web.Services.WebMethod(Description:="Returns a list of hits for the given ContentID.")> _
        Public Function GetHitListByContentID(ByVal ContentID As String) As [Global].SrchResults

            Dim rs As ADODB.Recordset
            Dim sr As [Global].SrchResults
            Dim hitcount As Integer = 0

            rs = DMS.GetHitListFromContentID(ContentID)
            If (rs Is Nothing) Then
                Throw New ArgumentException("Invalid Batch and/or sequence number specified")
            End If

            If Not (rs Is Nothing) Then
                ' If Max hits = 0 then return all results
                hitcount = rs.RecordCount
                sr = ParseSearchResults(rs, hitcount)
                sr.CacheID = 0

            Else
                sr.HitCount = 0
            End If

            Return sr

        End Function


        <System.Web.Services.WebMethod(Description:="Changes the index values for the Index Set identified by sIndexID.  Note: Field name/value pairs in the FieldValues() array are NOT position dependent.")> _
        Public Function ChangeDocLink(ByVal sIndexID As String, ByVal FieldValues() As [Global].DMSField, ByVal StartPage As Integer, ByVal EndPage As Integer, ByVal bImage As Boolean) As String

            Dim Names() As String
            Dim Values() As String
            Dim rc As String

            If (ParseFieldData(FieldValues, Names, Values)) Then
                rc = DMS.ChangeDocLink(sIndexID, Names, Values, bImage, StartPage, EndPage)
            Else
                Throw New ArgumentException("Invalid fields (or field lengths) specified")
            End If

            Return rc

        End Function


        <System.Web.Services.WebMethod(Description:="Uploads the contents of a string as a new Document to the DMS, returning the Batch and Sequence Number as a comma seperated string.  Note: Field name/value pairs in the FieldValues() array are NOT position dependent.")> _
            Public Function UploadString(ByVal DocClass As String, ByVal FieldValues() As [Global].DMSField, _
                                         ByVal Content As String, ByVal ContentType As String) As String

            Dim Values() As String

            If (ValidateFields(DocClass, FieldValues, Values) <> True) Then
                Throw New ArgumentException("Invalid fields (or field lengths) specified")
            End If

            Dim fs As FileStream
            Dim StringLength As Integer = Len(Content)
            Dim encoding As System.Text.ASCIIEncoding = New System.Text.ASCIIEncoding
            Dim buffer() As Byte = encoding.GetBytes(Content)
            Dim ftmp As String = GetTempFileName(ContentType)

            fs = File.Create(ftmp)
            fs.Write(buffer, 0, StringLength)
            fs.Close()

            Dim BatchSeq As String
            BatchSeq = DMS.UploadFile(DocClass, Values, ftmp)
            File.Delete(ftmp)
            Return BatchSeq

        End Function

        '===========================================================================================
        ' Add DocLink Support Functions
        '===========================================================================================
        Private Function ParseFieldData(ByVal FieldValues() As [Global].DMSField, ByRef Values() As String, ByRef Names() As String) As Boolean

            Dim DF As [Global].DMSField
            Dim fi As [Global].FieldInfo
            Dim rc As Boolean = True
            Try
                ' Allocate our values array
                ReDim Values(UBound(FieldValues))
                ReDim Names(UBound(FieldValues))

                Dim i As Integer
                For Each DF In FieldValues

                    Values(i) = Trim(DF.Value)
                    Names(i) = UCase(Trim(DF.Name))

                Next
            Catch ex As Exception
                TraceExceptions("ParseFieldData", ex)
                rc = False
            End Try


            Return rc
        End Function



        '===========================================================================================
        ' Upload File Support Functions
        '===========================================================================================
        Private Function ValidateFields(ByVal DocClass As String, ByVal FieldValues() As [Global].DMSField, ByRef Values() As String) As Boolean

            Dim FieldInfo() As [Global].FieldInfo
            FieldInfo = DMS.GetFieldInfo(DocClass)

            Dim DF As [Global].DMSField
            Dim fi As [Global].FieldInfo
            Dim foundname As Boolean
            Dim Validated As Boolean = True
            ' Allocate our values array
            ReDim Values(UBound(FieldValues))

            For Each DF In FieldValues ' our incoming values
                foundname = False
                For Each fi In FieldInfo ' our field defs from the dms
                    If UCase(Trim(fi.FieldName)) = UCase(Trim(DF.Name)) Then
                        If (DF.Value.Length > fi.FieldLen) Then
                            Validated = False
                            Exit For
                        End If
                        foundname = True
                        Exit For
                    End If
                Next
                If foundname = False Then
                    Validated = False
                    Exit For
                End If
            Next
            ' If we validated, copy the data into our array
            If Validated = True Then
                Dim i As Integer
                For Each DF In FieldValues
                    For i = LBound(FieldInfo) To UBound(FieldInfo)
                        If UCase(Trim(FieldInfo(i).FieldName)) = UCase(Trim(DF.Name)) Then
                            Values(i) = DF.Value
                        End If
                    Next
                Next
            End If

            Return Validated
        End Function
        '===========================================================================================
        ' Get/Put File Support Functions
        '===========================================================================================
        Function GetTempFileName(ByVal Type As String)

            Dim fn As String
            Dim dir As String
            Dim fullpath As String
            Dim TempDir As String
            Dim c As New Config

            TempDir = c.GetTempDir

            dir = HttpContext.Current.Server.MapPath("")

            Dim foundit As Boolean = False
            While (foundit = False)
                fn = Path.GetTempFileName
                fn = Path.GetFileNameWithoutExtension(fn)
                fullpath = dir + "\" + TempDir + "\" + fn + "." + Type
                If Not File.Exists(fullpath) Then
                    foundit = True
                End If
            End While
            Return fullpath
        End Function


        '===========================================================================================
        ' Search Support Functions
        '===========================================================================================
        Private Function PrepareSearch(ByRef Search As [Global].SearchCriteria) As DMSServer.DMSSearch
            Dim DSearch As DMSServer.DMSSearch
            Select Case Search.SearchType
                Case [Global].ESearchType.DocLinks
                    DSearch.SearchType = VIPDMS.SearchTypeEnum.stDocLink
                Case [Global].ESearchType.OmniLinks
                    DSearch.SearchType = VIPDMS.SearchTypeEnum.stOminLink
            End Select
            If (Search.FromDate Is Nothing) Then
                DSearch.FromDate = ""
            Else
                DSearch.FromDate = Search.FromDate
            End If

            If (Search.ToDate Is Nothing) Then
                DSearch.ToDate = ""
            Else
                DSearch.ToDate = Search.ToDate
            End If
            DSearch.FromDate = Search.FromDate
            DSearch.ToDate = Search.ToDate
            DSearch.Values = New Collection
            Dim i As Integer
            Dim f As DMSServer.DMSField
            For i = LBound(Search.Values) To UBound(Search.Values)
                f.Name = Search.Values(i).FieldName
                f.Value = Search.Values(i).FieldValue
                DSearch.Values.Add(f)
            Next
            DSearch.DocClass = Search.DocClass

            Return DSearch
        End Function
        Private Function GetFieldCount(ByRef rs As ADODB.Recordset) As Integer
            Dim FieldCount As Integer = 0
            rs.MoveFirst()
            Dim oField As ADODB.Field
            For Each oField In rs.Fields
                If (Mid(oField.Name, 1, 1) <> "~") Then
                    FieldCount = FieldCount + 1
                End If
            Next
            Return FieldCount
        End Function
        Private Function ParseSearchResults(ByRef rs As ADODB.Recordset, ByVal MaxHits As Integer) As [Global].SrchResults
            Dim sr As [Global].SrchResults
            Dim i As Integer
            Dim FieldCount As Integer
            sr.Start = 1
            sr.TotalHits = rs.RecordCount
            If rs.RecordCount > MaxHits Then
                sr.HitCount = MaxHits
            Else
                sr.HitCount = rs.RecordCount
            End If

            If (sr.HitCount > 0) Then
                ' Allocate some space to store the record items and fields
                FieldCount = GetFieldCount(rs)
                rs.MoveFirst()
                ReDim sr.Items(sr.HitCount - 1)
                For i = LBound(sr.Items) To UBound(sr.Items)
                    ReDim sr.Items(i).Fields(FieldCount - 1)
                Next

                Dim oField As ADODB.Field
                Dim fi As Integer ' Field Index
                Dim finished As Boolean = False
                For i = LBound(sr.Items) To UBound(sr.Items)
                    fi = 0
                    For Each oField In rs.Fields
                        'Only store the fields that DONT begin with "~"
                        If (Mid(oField.Name, 1, 1) = "~") Then
                            Select Case oField.Name
                                Case "~FileName"
                                    sr.Items(i).Meta.Filename = oField.Value.ToString
                                Case "~FileType"
                                    sr.Items(i).Meta.FileType = oField.Value.ToString
                                Case "~Comment"
                                    sr.Items(i).Meta.Comment = oField.Value.ToString
                                Case "~ContentType"
                                    sr.Items(i).Meta.ContentType = Val(oField.Value.ToString)
                                Case "~StartPage"
                                    sr.Items(i).Meta.StartPage = Val(oField.Value.ToString)
                                Case "~EndPage"
                                    sr.Items(i).Meta.EndPage = Val(oField.Value.ToString)
                                Case "~TotalPages"
                                    sr.Items(i).Meta.TotalPages = Val(oField.Value.ToString)
                                Case "~LockType"
                                    sr.Items(i).Meta.LockType = Val(oField.Value.ToString)
                                Case "~SeqNumber"
                                    sr.Items(i).Meta.SeqNo = oField.Value.ToString
                                Case "~FileNumber"
                                    sr.Items(i).Meta.Batch = oField.Value.ToString
                            End Select
                        Else
                            sr.Items(i).Fields(fi).Name = oField.Name
                            sr.Items(i).Fields(fi).Value = oField.Value.ToString
                            fi = fi + 1
                        End If

                    Next
                    rs.MoveNext()
                Next
            Else
                ' Done to prevent serialization errors when asking for more hits then
                ' exist in the cache
                ReDim sr.Items(1)

            End If

            Return sr
        End Function
        Private Function ParseSearchResultsEx(ByRef rs As ADODB.Recordset, ByVal StartRec As Long, ByVal Count As Long) As [Global].SrchResults

            Dim sr As [Global].SrchResults
            Dim i As Integer
            Dim FieldCount As Integer
            sr.TotalHits = rs.RecordCount


            If rs.RecordCount > (StartRec + Count) Then
                sr.HitCount = Count
            Else
                sr.HitCount = rs.RecordCount - StartRec
            End If

            If (sr.HitCount > 0) Then
                ' Allocate some space to store the record items and fields
                FieldCount = GetFieldCount(rs)
                ' Move to our starting record
                rs.Move(StartRec)
                ReDim sr.Items(sr.HitCount - 1)
                For i = LBound(sr.Items) To UBound(sr.Items)
                    ReDim sr.Items(i).Fields(FieldCount - 1)
                Next

                Dim oField As ADODB.Field
                Dim fi As Integer ' Field Index
                Dim finished As Boolean = False
                For i = LBound(sr.Items) To UBound(sr.Items)
                    fi = 0
                    For Each oField In rs.Fields
                        'Only store the fields that DONT begin with "~"
                        If (Mid(oField.Name, 1, 1) = "~") Then
                            Select Case oField.Name
                                Case "~FileName"
                                    sr.Items(i).Meta.Filename = oField.Value.ToString
                                Case "~FileType"
                                    sr.Items(i).Meta.FileType = oField.Value.ToString
                                Case "~Comment"
                                    sr.Items(i).Meta.Comment = oField.Value.ToString
                                Case "~ContentType"
                                    sr.Items(i).Meta.ContentType = Val(oField.Value.ToString)
                                Case "~StartPage"
                                    sr.Items(i).Meta.StartPage = Val(oField.Value.ToString)
                                Case "~EndPage"
                                    sr.Items(i).Meta.EndPage = Val(oField.Value.ToString)
                                Case "~TotalPages"
                                    sr.Items(i).Meta.TotalPages = Val(oField.Value.ToString)
                                Case "~LockType"
                                    sr.Items(i).Meta.LockType = Val(oField.Value.ToString)
                                Case "~SeqNumber"
                                    sr.Items(i).Meta.SeqNo = oField.Value.ToString
                                Case "~FileNumber"
                                    sr.Items(i).Meta.Batch = oField.Value.ToString
                            End Select
                        Else
                            sr.Items(i).Fields(fi).Name = oField.Name
                            sr.Items(i).Fields(fi).Value = oField.Value.ToString
                            fi = fi + 1
                        End If

                    Next
                    rs.MoveNext()
                Next
            End If

            Return sr
        End Function
    End Class

End Namespace
