Imports System.Web
Imports System.Web.SessionState


Public Class [Global]

    Inherits System.Web.HttpApplication
    Public Shared DMS As DMSWebAPI.DMSServer
    Public Shared oSession As VIPDMS.Session
    Public Shared Cfg As New DMSWebAPI.Config
    Public Shared GCache As DMSWebAPI.SRCache

    Structure FieldInfo

        Dim FieldName As String
        Dim FieldDesc As String
        Dim FieldLen As String
        Dim FieldValue As String

    End Structure
    Structure DMSField

        Dim Name As String
        Dim Value As String

    End Structure
    Structure ItemMetaData

        Dim Filename As String
        Dim FileType As String
        Dim Comment As String
        Dim ContentType As Integer
        Dim StartPage As Integer
        Dim EndPage As Integer
        Dim TotalPages As Integer
        Dim LockType As Integer
        Dim Batch As String
        Dim SeqNo As String

    End Structure
    Structure SrchItem
        Dim Fields() As DMSField
        Dim Meta As ItemMetaData
    End Structure
    Structure SrchResults
        Dim HitCount As Long
        Dim TotalHits As Long
        Dim Start As Long
        Dim Items() As SrchItem
        Dim CacheID As Long
    End Structure
    Public Enum ESearchType

        DocLinks = 0
        OmniLinks = 1

    End Enum
    Structure SearchCriteria

        Dim DocClass As String
        Dim SearchType As ESearchType
        Dim FromDate As String
        Dim ToDate As String
        Dim Values() As FieldInfo
        Dim MaxHits As Long
        Dim CacheResults As Boolean

    End Structure
    Public Enum EAppInfo

        aiVersion = 0
        aiCopyright = 1
        aiCompany = 2
        ' Forces a soap exception - for testing.
        aiException = 3

    End Enum
    Public Enum CacheOpStatus
        cosOK = 0
        cosItemNotInCache = 1
        cosCantGetReadLock = 2
        cosCantGetWriteLock = 3
        cosCantAddItem = 4
        cosCantRemoveItem = 5
        cosOtherError = 99

    End Enum

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        ' Initialize our global objects here.
        DMSWebAPI.Utility.DbgMsg("Global.Application_Start()")
        Cfg = New DMSWebAPI.Config
        DMS = New DMSWebAPI.DMSServer(Cfg.GetServer(), Cfg.GetDomain(), Cfg.GetUser(), Cfg.GetPass())
        oSession = DMS.GetSession
        GCache = New DMSWebAPI.SRCache(Context.Cache)
        Application.Add("DMSServer", DMS)
        Application.Add("DMSSession", oSession)
        Application.Add("Cache", GCache)

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class
