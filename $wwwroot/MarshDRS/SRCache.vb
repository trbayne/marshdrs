Imports System
Imports System.Threading
Imports Microsoft.VisualBasic


Namespace DMSWebAPI


    Public Class SRCache

        Private Shared ContextCache As System.Web.Caching.Cache = Nothing
        Private Shared NextCacheID As Long = 1
        Private Shared LastExceptionMsg As String
        Private Shared rwl As New ReaderWriterLock
        Private Const ReadLockTimeout As Long = 100
        Private Const WriteLockTimeout As Long = 100



        Public Sub New(ByRef CC As System.Web.Caching.Cache)

            If (ContextCache Is Nothing) Then
                ContextCache = CC
                NextCacheID = 1
            End If

        End Sub

        Public Function AddRecordSet(ByRef rs As ADODB.Recordset, ByRef id As Long) As [Global].CacheOpStatus

            Dim CIDString As String
            Dim RC As [Global].CacheOpStatus = [Global].CacheOpStatus.cosOK

            Try
                rwl.AcquireWriterLock(WriteLockTimeout)
                id = NextCacheID
                NextCacheID = NextCacheID + 1
                Try
                    CIDString = "DWA_RS-" & Str(id)
                    ContextCache.Insert(CIDString, rs, Nothing, DateTime.MaxValue, TimeSpan.FromSeconds([Global].Cfg.GetCacheTimeout))
                    RC = SetLastError([Global].CacheOpStatus.cosOK)
                Catch ex As Exception
                    TraceExceptions("AddRecordSet()", ex)
                    RC = SetLastError([Global].CacheOpStatus.cosCantAddItem)
                Finally
                    ' Ensure that the lock is released.
                    rwl.ReleaseWriterLock()
                End Try
            Catch ex As ApplicationException
                ' The writer lock request timed out.
                RC = [Global].CacheOpStatus.cosCantGetWriteLock
                LastExceptionMsg = GetErrorMsg([Global].CacheOpStatus.cosCantGetWriteLock)

            End Try
            Return RC


        End Function

        Public Function GetRecordSet(ByVal id As Long, ByRef rs As ADODB.Recordset) As [Global].CacheOpStatus

            Dim CIDString As String
            Dim RC As [Global].CacheOpStatus = [Global].CacheOpStatus.cosOK

            Try
                rwl.AcquireReaderLock(ReadLockTimeout)
                Try
                    CIDString = "DWA_RS-" & Str(id)
                    rs = ContextCache(CIDString)
                    RC = SetLastError([Global].CacheOpStatus.cosOK)

                Catch ex As Exception
                    TraceExceptions("GetRecordSet()", ex)
                    RC = SetLastError([Global].CacheOpStatus.cosItemNotInCache)
                Finally
                    rwl.ReleaseReaderLock()

                End Try

            Catch ex As ApplicationException
                ' The reader lock request timed out.
                TraceExceptions("GetRecordSet()", ex)
                RC = SetLastError([Global].CacheOpStatus.cosCantGetReadLock)

            End Try

            Return RC

        End Function
        Private Shared Function SetLastError(ByVal ErrorType As [Global].CacheOpStatus) As [Global].CacheOpStatus
            LastExceptionMsg = GetErrorMsg(ErrorType)
            Return ErrorType
        End Function
        Private Shared Function GetErrorMsg(ByVal ErrorType As [Global].CacheOpStatus) As String
            Dim Msg As String
            Select Case ErrorType
                Case [Global].CacheOpStatus.cosCantAddItem
                    Msg = "Error adding item to cache"
                Case [Global].CacheOpStatus.cosCantGetReadLock
                    Msg = "Can't acquire read lock"
                Case [Global].CacheOpStatus.cosCantGetWriteLock
                    Msg = "Can't Acquire write lock"
                Case [Global].CacheOpStatus.cosCantRemoveItem
                    Msg = "Can't Remove Item"
                Case [Global].CacheOpStatus.cosItemNotInCache
                    Msg = "Can't locate item in the cache"
                Case [Global].CacheOpStatus.cosOK
                    Msg = "Cache Operation OK"
                Case [Global].CacheOpStatus.cosOtherError
                    Msg = "Unspecified Cache error occured"
                Case Else
                    Msg = "Unknown error occured"
            End Select
            Return Msg
        End Function

        Public Function GetLastExceptionMsg() As String
            Return LastExceptionMsg
        End Function

    End Class

End Namespace
