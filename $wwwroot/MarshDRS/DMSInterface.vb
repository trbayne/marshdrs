Imports System.Globalization


Namespace DMSWebAPI

    Public Class DMSServer

        Private Shared DMSApp As New VIPDMS.Application
        Private Shared LoggedIn As Boolean = False
        Private Shared oSession As VIPDMS.Session
        Private Shared oIndexDefs() As VIPDMS.IndexInfo = Nothing
        Private Shared mServer As String
        Private Shared mDomain As String
        Private Shared mUser As String
        Private Shared mPass As String

        Public Structure DMSField
            Dim Name As String
            Dim Value As String
        End Structure
        Public Structure DMSSearch

            Dim SearchType As VIPDMS.SearchTypeEnum
            Dim DocClass As String
            Dim FromDate As String
            Dim ToDate As String
            Dim Values As Collection

        End Structure
        Public Sub New(ByVal Server As String, ByVal Domain As String, ByVal User As String, ByVal Pass As String)
            mServer = Server
            mDomain = Domain
            mUser = User
            mPass = Pass

        End Sub
        Public Function Version() As String
            Dim rc As String
            Try
                rc = DMSApp.Version
                Return rc
            Catch ex As Exception
                TraceExceptions("Version", ex)
            End Try


        End Function
        Public Sub Refresh()

            Dim colDocLinks As VBA.Collection ' This collection will contain
            Dim oIndexDef As VIPDMS.IndexInfo
            ' the list of DocLinks from the
            ' DMS Server
            Try
                Erase oIndexDefs
                ' Get the list of DocLinks from the server
                colDocLinks = oSession.IndexDefs(VIPDMS.dcTypeEnum.dctAll)
                ' Enumerate the DocLinks and load them into the Listbox
                Dim i As Integer
                i = 0
                For Each oIndexDef In colDocLinks
                    ReDim Preserve oIndexDefs(i + 1)
                    oIndexDefs(i) = oIndexDef
                    i = i + 1
                Next
            Catch ex As Exception
                TraceExceptions("GetIndexInfo", ex)
            End Try

        End Sub
        Public Shared Function RefreshSession()
            Try

                DMSApp = Nothing
                DMSApp = New VIPDMS.Application
                If Not (oSession Is Nothing) Then
                    LoggedIn = False
                    oSession.CloseSession()
                    oSession = Nothing
                    oSession = DMSApp.ConnectToServer(mServer, mUser, mPass, mDomain)
                    If Not (oSession Is Nothing) Then
                        LoggedIn = True
                    End If
                End If
            Catch ex As Exception
                TraceExceptions("RefreshSession()", ex)
            End Try

        End Function
        Public Shared Function GetSession() As VIPDMS.Session

            'Only create a session if one doesn't already exist!
            If oSession Is Nothing Then
                Try
                    oSession = DMSApp.ConnectToServer(mServer, mUser, mPass, mDomain)

                    If Not (oSession Is Nothing) Then
                        LoggedIn = True
                    End If
                Catch ex As Exception
                    TraceExceptions("GetSession", ex)
                End Try

            End If

            Return oSession

        End Function
        Public Function UploadFile(ByVal DocClass As String, ByVal FieldValues() As String, ByVal FileName As String) As String

            Dim BatchSeq As String
            Dim oSession As VIPDMS.Session
            BatchSeq = "UPLOAD FAILED"
            Try
                oSession = GetSession()
                BatchSeq = oSession.UploadDocument(DocClass, FileName, "", FieldValues, False)

            Catch ex As Exception
                TraceExceptions("UploadFile", ex)
            End Try

            Return BatchSeq

        End Function
        Public Function ChangeDocLink(ByRef sIndexID As String, ByVal FieldNames() As String, ByVal FieldValues() As String, ByVal bImage As Boolean, ByVal iStartPage As Integer, ByVal iEndPage As Integer) As String

            Dim rc As String
            Dim oSession As VIPDMS.Session

            rc = "Change DocLink Failed"
            Try
                oSession = GetSession()
                rc = oSession.ChangeDocLink(sIndexID, FieldNames, FieldValues, bImage, iStartPage, iEndPage)
            Catch ex As Exception
                TraceExceptions("ChangeDocLink", ex)
            End Try


        End Function
        Public Function RemoveDocLink(ByVal sIndexID As String, ByVal bImage As Boolean)

            Dim rc As String
            Dim oSession As VIPDMS.Session

            rc = "Delete DocLink Failed"
            Try
                oSession = GetSession()
                rc = oSession.DeleteDocLink(sIndexID, bImage)
            Catch ex As Exception
                TraceExceptions("DeleteDocLink", ex)
            End Try


        End Function
        Public Function AddDocLink(ByVal ContentID As String, ByVal FieldNames() As String, ByVal FieldValues() As String, ByVal bImage As Boolean, ByVal StartPage As Integer, ByVal EndPage As Integer) As String

            Dim rc As String
            Dim oSession As VIPDMS.Session

            rc = "ADD DOCLINK FAILED"
            Try
                oSession = GetSession()
                rc = oSession.AddDocLink(ContentID, FieldNames, FieldValues, bImage, StartPage, EndPage)
            Catch ex As Exception
                TraceExceptions("AddDocLink", ex)
            End Try

            Return rc

        End Function
        Public Shared Function BuildIndexID(ByVal DocClass As String, ByVal BatchNum As String, ByVal Seq As String, ByVal DocLinkSeq As String)
            Dim rc As String
            rc = "/" + DocClass + "/" + BatchNum + "/" + Seq + "/" + DocLinkSeq
            Return rc
        End Function
        Public Shared Function BuildContentID(ByVal DocClass As String, ByVal BatchNum As String, ByVal Seq As String) As String
            Dim rc As String
            rc = "/" + DocClass + "/" + BatchNum + "/" + Seq
            Return rc
        End Function
        Public Shared Function GetHitListFromContentID(ByVal ContentID As String) As ADODB.Recordset

            Dim oSession As VIPDMS.Session
            Dim rs As ADODB.Recordset

            Try
                oSession = GetSession()
                rs = oSession.HitListFromContentID(ContentID)

                Return rs

            Catch ex As Exception
                TraceExceptions("GetHitListFromContentID", ex)
                Throw ex
            End Try

        End Function

        Public Shared Function GetDocClasses() As String()

            Dim rc() As String
            Dim colDocLinks As VBA.Collection ' This collection will contain

            Try
                oSession = DMSServer.GetSession()
                colDocLinks = oSession.IndexDefs(VIPDMS.dcTypeEnum.dctAll)

                ' Enumerate the DocLinks and load them into the Listbox
                Dim i As Integer = 0
                Dim oIndexDef As VIPDMS.IndexInfo
                For Each oIndexDef In colDocLinks
                    ReDim Preserve rc(i)
                    rc(i) = oIndexDef.Name()
                    i = i + 1
                Next

            Catch ex As Exception
                TraceExceptions("GetDocClasses", ex)
            End Try

            Return rc
        End Function

        Public Shared Function GetFieldInfo(ByVal DocClass As String) As [Global].FieldInfo()

            Dim fi() As [Global].FieldInfo
            Dim Status As Boolean = False
            Dim colDocLinks As VBA.Collection ' This collection will contain
            Dim oIndexDef As VIPDMS.IndexInfo
            Dim idef As VIPDMS.IndexInfo
            Dim ic As VIPDMS.IndexCriteria
            Dim i As Integer
            Dim bFoundDocClass As Boolean = False

            Try
                For i = LBound(oIndexDefs) To UBound(oIndexDefs)
                    idef = oIndexDefs(i)
                    If Not idef Is Nothing Then
                        If (UCase(idef.Name) = UCase(DocClass)) Then
                            bFoundDocClass = True
                            Exit For
                        End If
                    End If
                Next
                If (bFoundDocClass = True) Then

                    ReDim fi(idef.CriteriaCount - 2)
                    ' 1 based array, skip the CREATED field
                    For i = 2 To idef.CriteriaCount
                        ic = idef.Criteria(i)
                        fi(i - 2).FieldName = ic.Name
                        fi(i - 2).FieldDesc = ic.Description
                        fi(i - 2).FieldLen = ic.Length

                    Next i
                End If
            Catch ex As Exception
                TraceExceptions("GetFldDefList", ex)
            End Try

            Return fi
        End Function
        
        Public Function ExecuteSearch(ByVal Search As DMSSearch) As ADODB.Recordset


            Dim Field As DMSField
            Dim oSearch As VIPDMS.Search
            Dim rs As ADODB.Recordset

            Try

                ' Now from the session, get a Search object
                oSearch = oSession.CreateSearch(Search.DocClass, Search.SearchType)
                'Catch
                '    oSearch.RefreshDocClassList()

                '    DbgMsg("DMSInterface:ExecuteSearch(): CreateSearch failed, updating docclass list")
                'End Try

                ''Try
                'If (oSearch Is Nothing) Then
                '    oSearch = oSession.CreateSeach(Search.DocClass, Search.SearchType)
                'End If

                ' Add our date values
                oSearch.FromDate = FormatDate(Search.FromDate, Date.MinValue)
                oSearch.ToDate = FormatDate(Search.ToDate, Date.MaxValue)

                For Each Field In Search.Values
                    oSearch.Criteria(Field.Name) = Field.Value
                Next
                rs = oSearch.DoSearch()
                Return rs

            Catch ex As Exception
                TraceExceptions("DoSearch", ex)
            End Try

        End Function
        Public Function GetImage(ByVal Filename As String, ByVal Item As [Global].ItemMetaData) As Boolean
            Dim rc = True
            Try
                With Item
                    oSession.GetImageToFile(.ContentType, .Batch, Val(.SeqNo), .StartPage, .EndPage, .TotalPages, Filename)
                End With
            Catch ex As Exception
                rc = False
                TraceExceptions("GetImage()", ex)
            End Try
            Return rc
        End Function
        Private Function FormatDate(ByVal MyDate As String, ByVal DefaultValue As Date) As Date

            Dim MyRealDate As Date
            If Not (MyDate Is Nothing) Then
                Dim MyCultureInfo As CultureInfo = New CultureInfo("en-US")
                MyRealDate = Date.ParseExact(MyDate, "yyyyMMdd", MyCultureInfo)
            Else
                MyRealDate = DefaultValue
            End If
            Return MyRealDate
        End Function
    End Class

End Namespace
