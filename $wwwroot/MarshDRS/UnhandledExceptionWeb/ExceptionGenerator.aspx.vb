Public Class ExceptionGenerator
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        SubOne()
    End Sub

    Private Sub GenerateException()
        Dim nvc As Specialized.NameValueCollection
        TextBox1.Text = nvc.Count
    End Sub

    Private Sub SubOne()
        Try
            SubTwo()
        Catch ex As Exception
            Throw New InvalidCastException("SubOne call failed!", ex)
        End Try
    End Sub

    Private Sub SubTwo()
        Try
            GenerateException()
        Catch ex As Exception
            Throw New DataException("SubTwo handled an exception!", ex)
        End Try
    End Sub

End Class
