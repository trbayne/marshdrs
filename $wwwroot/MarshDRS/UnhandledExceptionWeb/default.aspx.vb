Imports System.Text

Public Class WebForm1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents HyperLink1 As System.Web.UI.WebControls.HyperLink
    Protected WithEvents Hyperlink2 As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim dt As New DataTable("TestTable")
        dt.Columns.Add("Col1")
        dt.Columns.Add("Col2")
        Dim dr As DataRow = dt.NewRow
        dr.Item(0) = "1"
        dr.Item(1) = "2"
        dt.Rows.Add(dr)
        Application.Add("Test.DataTable", dt)

        Dim sc As New Specialized.StringCollection
        sc.Add("string1")
        sc.Add("string2")
        Cache.Add("Test.StringCollection", sc, Nothing, DateTime.Now.AddMinutes(2), TimeSpan.Zero, _
            Caching.CacheItemPriority.Normal, Nothing)

        Dim ht As New Hashtable
        ht.Add("key1", "val1")
        ht.Add("key2", "val2")
        Dim nvc As Specialized.NameValueCollection
        Dim nvc2 As New Specialized.NameValueCollection

        Session.Add("Test.HashTable", ht)
        Session.Add("Test.String", "Hello World!")
        Session.Add("Test.Integer", 42)
        Session.Add("Test.DateTime", DateTime.Now)
        Session.Add("Test.Boolean", True)
        Session.Add("Test.NameValueCollection", nvc)
        Session.Add("Test.NameValueCollection2", nvc2)

    End Sub

End Class
