<%@ Page Language="vb" AutoEventWireup="false" Codebehind="default.aspx.vb" Inherits="UnhandledExceptionWeb.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ASP.NET Unhandled Exception Demo</title>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<P>This is a&nbsp;basic demonstration&nbsp;of the <STRONG>ASPUnhandledException</STRONG>
				class. For complete documentation check the source and the <A href="http://www.codeproject.com/aspnet/ASPNETExceptionHandling.asp">
					CodeProject article</A>.</P>
			<ul>
				<li>
					<asp:HyperLink id="HyperLink1" runat="server" NavigateUrl="ExceptionGenerator.aspx">This page generates an unhandled exception</asp:HyperLink>
				<li>
					<asp:HyperLink id="Hyperlink2" runat="server" NavigateUrl="AlternateExceptionGenerator.aspx">This page generates an unhandled exception with ViewState</asp:HyperLink>&nbsp;</li>
			</ul>
			Compare&nbsp;screenshots of the default ASP.NET default unhandled exception web 
			pages:
			<ol>
				<Li>
					<A href="images/screenshot_aspnet_unhandled_default.gif" target="_blank">default 
						ASP.NET Unhandled Exception page</A>
				<li>
					<A href="images/screenshot_aspnet_unhandled_remoteonly.gif" target="_blank">default 
						ASP.NET RemoteOnly Unhandled Exception page</A>&nbsp;</li></ol>
			<P>Here are some troubleshooting tips for specific notification types:
			</P>
			<P>
				<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="1">
					<TR>
						<TH width="150">
							Notification Type</TH>
						<TH width="*">
							Required Configuration</TH>
					</TR>
					<TR>
						<TD>email</TD>
						<TD>make sure the SMTP settings are correct in your web.config.&nbsp;If SMTP is 
							misconfigured, it may take a while for the exception page to display (the page 
							can't render until the email has either sent or failed to send, and it retries 
							3 times by default.)</TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 22px">Event Log</TD>
						<TD style="HEIGHT: 22px">you must give the ASP.NET worker process write permission 
							to the registry</TD>
					</TR>
					<TR>
						<TD>Text File</TD>
						<TD>you must give the ASP.NET worker process write permission to your website root</TD>
					</TR>
					<TR>
						<TD>UI (web page)</TD>
						<TD>no configuration required; this automatically works with any 
							CustomErrors&nbsp;value. You are not required to use Off. If you want your own 
							custom error UI, be sure to turn this off using the LogToUi config key.</TD>
					</TR>
				</TABLE>
			</P>
		</form>
	</body>
</HTML>
