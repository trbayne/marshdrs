<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AlternateExceptionGenerator.aspx.vb" Inherits="UnhandledExceptionWeb.AlternateExceptionGenerator"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AlternateExceptionGenerator</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:Button id="Button1" runat="server" Text="Click Me To Generate An Exception"></asp:Button><BR>
			<BR>
			<asp:TextBox id="TextBox1" runat="server" Width="296px" Height="22px">The Quick Brown Fox Jumps Over The Lazy Dog</asp:TextBox><BR>
			<BR>
			<asp:DropDownList id="DropDownList1" runat="server">
				<asp:ListItem Value="Item1">Item1</asp:ListItem>
				<asp:ListItem Value="Item2">Item2</asp:ListItem>
				<asp:ListItem Value="Item3">Item3</asp:ListItem>
				<asp:ListItem Value="Item4">Item4</asp:ListItem>
				<asp:ListItem Value="Item5">Item5</asp:ListItem>
				<asp:ListItem Value="Item6">Item6</asp:ListItem>
				<asp:ListItem Value="Item7">Item7</asp:ListItem>
				<asp:ListItem Value="Item8">Item8</asp:ListItem>
			</asp:DropDownList><BR>
			<BR>
			<asp:Calendar id="Calendar1" runat="server" SelectedDate="2004-12-18" VisibleDate="2004-12-25"
				SelectionMode="DayWeekMonth" ToolTip="This is a Calendar" Font-Names="Tahoma" Font-Size="9pt"
				NextPrevFormat="ShortMonth" ShowGridLines="True" DayNameFormat="FirstLetter" BorderStyle="Dashed"
				BorderWidth="3px" BackColor="#C0C0FF" BorderColor="Olive"></asp:Calendar><BR>
			<asp:ListBox id="ListBox1" runat="server">
				<asp:ListItem Value="Item1">Item1</asp:ListItem>
				<asp:ListItem Value="Item2">Item2</asp:ListItem>
				<asp:ListItem Value="Item3">Item3</asp:ListItem>
				<asp:ListItem Value="Item4">Item4</asp:ListItem>
				<asp:ListItem Value="Item5">Item5</asp:ListItem>
			</asp:ListBox>
		</form>
	</body>
</HTML>
