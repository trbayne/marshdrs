To build or modify this project you need to do the following:

1.  Create a IIS Virtual Directory and point it a the $wwwroot\DMSWebAPI directory, be sure and give it read, write, 
and execute permissions.  At the very least the directory specified by the TEMPDIR setting in the web.config file
MUST have read and write permissions.

2.  Install the VIPDMS control (VIPDMS.DLL and RetrieveContent.dll - both need to be registered).

3.  If you don't have the Microsoft Web Service Extensions 2.0 (WSE) installed, install it from 
the file "Microsoft WSE 2.0 SP2.msi" located in the ThirdParty directory.

4.  Open up the DMSWebAPI.sln with .Net, it will attempt to contact the server, and will eventually
display a dialog saying something about not being able to find the share it needs.  Here, select a 
new share, pointing it at your development directory\DMSWebAPI\$webroot\DMSWEBAPI

5.  Finally, copy the web.config file to $wwwroot/DMSWebAPI/web.config change the following sections to match your
configuration before you attempt to run the Service:

<appSettings>
    <!-- <add key="DMSSERVER" value="192.168.255.221"/> -->
    <!-- <add key="DMSSERVER" value="VIPDMS"/> -->
    <add key="DMSSERVER" value="VIPDMS" />
    <add key="DMSUSER" value="SWADMIN" />
    <add key="DMSPASS" value="swadmin" />
    <add key="DMSDOMAIN" value="DMS" />
    <add key="TEMPDIR" value="TEMPFILES" />
    <add key="DEBUG" value="True" />
    <add key="CACHETIMEOUT" value="120" />
  </appSettings>
  <UnhandledException>
    <add key="EmailTo" value="tbayne@opentext.com" />
    <add key="ContactInfo" value="Terry Bayne 315-946-6254" />
    <add key="IgnoreRegex" value="get_aspx_ver\.aspx" />
    <add key="SmtpDefaultDomain" value="tbayne.net" />
    <add key="SmtpServer" value="tbayne.net" />
    <add key="LogToEventLog" value="True" />
    <add key="LogToEmail" value="True" />
    <add key="IgnoreDebug" value="False" />
  </UnhandledException>
  
  