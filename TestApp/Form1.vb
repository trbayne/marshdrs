Imports System.IO
Imports Microsoft.Web.Services2
Imports Microsoft.Web.Services2.Dime
Public Class Form1
    Inherits System.Windows.Forms.Form

    Public Declare Function GetTickCount Lib "kernel32" () As Long

    Dim DMS As New DMSWEBAPI.DMSAPIWse
    Dim fi() As DMSWEBAPI.FieldInfo
    'Dim sr As DMSWEBAPI.SrchResults
    Dim Lbls() As Label
    Dim Values() As TextBox
    Dim SelectedSearchIndex As Integer = -1
    Dim sr As DMSWEBAPI.SrchResults
    Dim CacheID As Long
    Dim LastRec As Long


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnGetServiceInfo As System.Windows.Forms.Button
    Friend WithEvents btnGenerateSoapException As System.Windows.Forms.Button
    Friend WithEvents btnGetDocClassList As System.Windows.Forms.Button
    Friend WithEvents lbDocClasses As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pnlSearchParms As System.Windows.Forms.Panel
    Friend WithEvents tt As System.Windows.Forms.ToolTip
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents lbSearchResults As System.Windows.Forms.ListBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents nudMaxHits As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnRetrieveFile As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tbAppInfo As System.Windows.Forms.ListBox
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents btnUploadFile As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnGetNextHits As System.Windows.Forms.Button
    Friend WithEvents cbCacheResults As System.Windows.Forms.CheckBox
    Friend WithEvents btnUploadString As System.Windows.Forms.Button
    Friend WithEvents txtUploadString As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tbDocID As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnGetServiceInfo = New System.Windows.Forms.Button
        Me.btnGenerateSoapException = New System.Windows.Forms.Button
        Me.btnGetDocClassList = New System.Windows.Forms.Button
        Me.lbDocClasses = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.pnlSearchParms = New System.Windows.Forms.Panel
        Me.btnSearch = New System.Windows.Forms.Button
        Me.btnUploadFile = New System.Windows.Forms.Button
        Me.btnRetrieveFile = New System.Windows.Forms.Button
        Me.tt = New System.Windows.Forms.ToolTip(Me.components)
        Me.lbSearchResults = New System.Windows.Forms.ListBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.nudMaxHits = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.tbAppInfo = New System.Windows.Forms.ListBox
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.Button1 = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.btnGetNextHits = New System.Windows.Forms.Button
        Me.cbCacheResults = New System.Windows.Forms.CheckBox
        Me.btnUploadString = New System.Windows.Forms.Button
        Me.txtUploadString = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tbDocID = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        CType(Me.nudMaxHits, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(232, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Status/Info:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnGetServiceInfo
        '
        Me.btnGetServiceInfo.Location = New System.Drawing.Point(16, 24)
        Me.btnGetServiceInfo.Name = "btnGetServiceInfo"
        Me.btnGetServiceInfo.Size = New System.Drawing.Size(160, 32)
        Me.btnGetServiceInfo.TabIndex = 2
        Me.btnGetServiceInfo.Text = "1 - Get Web Service Info"
        '
        'btnGenerateSoapException
        '
        Me.btnGenerateSoapException.Location = New System.Drawing.Point(16, 504)
        Me.btnGenerateSoapException.Name = "btnGenerateSoapException"
        Me.btnGenerateSoapException.Size = New System.Drawing.Size(160, 32)
        Me.btnGenerateSoapException.TabIndex = 3
        Me.btnGenerateSoapException.Text = "Generate Soap Exception"
        '
        'btnGetDocClassList
        '
        Me.btnGetDocClassList.Location = New System.Drawing.Point(16, 72)
        Me.btnGetDocClassList.Name = "btnGetDocClassList"
        Me.btnGetDocClassList.Size = New System.Drawing.Size(160, 32)
        Me.btnGetDocClassList.TabIndex = 4
        Me.btnGetDocClassList.Text = "2 - Get DocClass List"
        '
        'lbDocClasses
        '
        Me.lbDocClasses.Location = New System.Drawing.Point(16, 280)
        Me.lbDocClasses.Name = "lbDocClasses"
        Me.lbDocClasses.Size = New System.Drawing.Size(336, 21)
        Me.lbDocClasses.Sorted = True
        Me.lbDocClasses.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(16, 256)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(336, 23)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Document Class List:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSearchParms
        '
        Me.pnlSearchParms.AutoScroll = True
        Me.pnlSearchParms.AutoScrollMargin = New System.Drawing.Size(5, 5)
        Me.pnlSearchParms.BackColor = System.Drawing.SystemColors.Info
        Me.pnlSearchParms.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlSearchParms.Location = New System.Drawing.Point(16, 320)
        Me.pnlSearchParms.Name = "pnlSearchParms"
        Me.pnlSearchParms.Size = New System.Drawing.Size(336, 88)
        Me.pnlSearchParms.TabIndex = 10
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(16, 464)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(160, 32)
        Me.btnSearch.TabIndex = 11
        Me.btnSearch.Text = "3 - Execute Search"
        '
        'btnUploadFile
        '
        Me.btnUploadFile.Location = New System.Drawing.Point(416, 472)
        Me.btnUploadFile.Name = "btnUploadFile"
        Me.btnUploadFile.Size = New System.Drawing.Size(160, 32)
        Me.btnUploadFile.TabIndex = 12
        Me.btnUploadFile.Text = "Upload A File"
        '
        'btnRetrieveFile
        '
        Me.btnRetrieveFile.Enabled = False
        Me.btnRetrieveFile.Location = New System.Drawing.Point(416, 424)
        Me.btnRetrieveFile.Name = "btnRetrieveFile"
        Me.btnRetrieveFile.Size = New System.Drawing.Size(160, 32)
        Me.btnRetrieveFile.TabIndex = 13
        Me.btnRetrieveFile.Text = "Retrieve File"
        '
        'lbSearchResults
        '
        Me.lbSearchResults.BackColor = System.Drawing.SystemColors.Info
        Me.lbSearchResults.HorizontalScrollbar = True
        Me.lbSearchResults.Location = New System.Drawing.Point(416, 288)
        Me.lbSearchResults.Name = "lbSearchResults"
        Me.lbSearchResults.Size = New System.Drawing.Size(336, 121)
        Me.lbSearchResults.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(416, 256)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(336, 23)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Search Results:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudMaxHits
        '
        Me.nudMaxHits.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaxHits.Location = New System.Drawing.Point(288, 424)
        Me.nudMaxHits.Name = "nudMaxHits"
        Me.nudMaxHits.Size = New System.Drawing.Size(64, 22)
        Me.nudMaxHits.TabIndex = 16
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(216, 424)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 24)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Max Hits:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbAppInfo
        '
        Me.tbAppInfo.BackColor = System.Drawing.SystemColors.Info
        Me.tbAppInfo.ContextMenu = Me.ContextMenu1
        Me.tbAppInfo.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbAppInfo.HorizontalScrollbar = True
        Me.tbAppInfo.ItemHeight = 15
        Me.tbAppInfo.Location = New System.Drawing.Point(344, 24)
        Me.tbAppInfo.Name = "tbAppInfo"
        Me.tbAppInfo.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.tbAppInfo.Size = New System.Drawing.Size(408, 94)
        Me.tbAppInfo.TabIndex = 18
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Copy"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(264, 48)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(48, 24)
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "(Clear)"
        '
        'btnGetNextHits
        '
        Me.btnGetNextHits.Enabled = False
        Me.btnGetNextHits.Location = New System.Drawing.Point(192, 464)
        Me.btnGetNextHits.Name = "btnGetNextHits"
        Me.btnGetNextHits.Size = New System.Drawing.Size(160, 32)
        Me.btnGetNextHits.TabIndex = 20
        Me.btnGetNextHits.Text = "Get Next Set of Hits"
        '
        'cbCacheResults
        '
        Me.cbCacheResults.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCacheResults.Location = New System.Drawing.Point(16, 424)
        Me.cbCacheResults.Name = "cbCacheResults"
        Me.cbCacheResults.Size = New System.Drawing.Size(184, 24)
        Me.cbCacheResults.TabIndex = 21
        Me.cbCacheResults.Text = "Cache Hit Results (on server)"
        '
        'btnUploadString
        '
        Me.btnUploadString.Location = New System.Drawing.Point(584, 424)
        Me.btnUploadString.Name = "btnUploadString"
        Me.btnUploadString.Size = New System.Drawing.Size(160, 32)
        Me.btnUploadString.TabIndex = 22
        Me.btnUploadString.Text = "Upload a String"
        '
        'txtUploadString
        '
        Me.txtUploadString.Location = New System.Drawing.Point(584, 464)
        Me.txtUploadString.Multiline = True
        Me.txtUploadString.Name = "txtUploadString"
        Me.txtUploadString.Size = New System.Drawing.Size(192, 72)
        Me.txtUploadString.TabIndex = 23
        Me.txtUploadString.Text = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.tbDocID)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 136)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(736, 104)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = " DocID Operations"
        '
        'tbDocID
        '
        Me.tbDocID.Location = New System.Drawing.Point(72, 24)
        Me.tbDocID.Name = "tbDocID"
        Me.tbDocID.Size = New System.Drawing.Size(168, 20)
        Me.tbDocID.TabIndex = 0
        Me.tbDocID.Text = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(24, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "DocID:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(24, 64)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(104, 24)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Get DocClass"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(792, 550)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtUploadString)
        Me.Controls.Add(Me.btnUploadString)
        Me.Controls.Add(Me.cbCacheResults)
        Me.Controls.Add(Me.btnGetNextHits)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbAppInfo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.nudMaxHits)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lbSearchResults)
        Me.Controls.Add(Me.btnRetrieveFile)
        Me.Controls.Add(Me.btnUploadFile)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.pnlSearchParms)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lbDocClasses)
        Me.Controls.Add(Me.btnGetDocClassList)
        Me.Controls.Add(Me.btnGenerateSoapException)
        Me.Controls.Add(Me.btnGetServiceInfo)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.nudMaxHits, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tt.SetToolTip(btnGetServiceInfo, "Click to get information about the DMSWEBAPI Web Service")
        tt.SetToolTip(btnGetDocClassList, "Click to get the list of Document Classes")
        tt.SetToolTip(lbDocClasses, "List of available document classes")
        tt.SetToolTip(pnlSearchParms, "Fields available in currently selected Document Class")
        tt.SetToolTip(btnSearch, "Execute a search for the current Document class, using values from the field list")
        tt.SetToolTip(btnGenerateSoapException, "Force the Web Service to generate a soap exception")
        tt.SetToolTip(nudMaxHits, "Maximum number of hits to return when searching")
        tt.SetToolTip(btnRetrieveFile, "Retrieve image/file for the record selected above")
        tt.SetToolTip(btnUploadFile, "Upload a file using the index values specified in the field list")
    End Sub
    Private Sub btnGetServiceInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetServiceInfo.Click

        Dim Version As String
        Dim Company As String
        Dim Copyright As String
        Dim URL As String
        Try
            URL = DMS.Url
            Version = DMS.AppInfo(DMSWEBAPI.EAppInfo.aiVersion)
            Copyright = DMS.AppInfo(DMSWEBAPI.EAppInfo.aiCopyright)
            Company = DMS.AppInfo(DMSWEBAPI.EAppInfo.aiCompany)
            ' Uncomment the following line to force generation of a SOAP Exception
            'Test = DMS.AppInfo(DMSWEBAPI.EAppInfo.aiException)

            Dim Info As String
            tbAppInfo.Items.Clear()
            AddToInfo("DMSWebAPI Info:")
            AddToInfo("           URL: " + URL)
            AddToInfo("       Version: " + Version)
            AddToInfo("       Company: " + Company)
            AddToInfo("     Copyright: " + Copyright)
            AddToInfo("VIPDMS Version: " + DMS.Version)
           ' Uncomment the following line to force generation of a SOAP Exception
            'Test = DMS.AppInfo(DMSWEBAPI.EAppInfo.aiException)

        Catch ex As System.Web.Services.Protocols.SoapException
            DisplaySoapException(ex)
        End Try
    End Sub


    Private Sub btnGenerateSoapException_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateSoapException.Click
        Dim Test As String

        Try
            Test = DMS.AppInfo(DMSWEBAPI.EAppInfo.aiException)
            tbAppInfo.Text = Test

        Catch ex As System.Web.Services.Protocols.SoapException
            DisplaySoapException(ex)
        End Try
    End Sub

    Private Sub btnGetDocClassList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetDocClassList.Click
        Dim DocClasses() As String
        Try
            DocClasses = DMS.GetDocClassList()
            Dim i As Integer
            Dim DocClass As String
            lbDocClasses.Items.Clear()
            For i = LBound(DocClasses) To UBound(DocClasses)
                lbDocClasses.Items.Add(DocClasses(i))
            Next
            If (lbDocClasses.Items.Count > 0) Then
                lbDocClasses.SelectedIndex = 0
            End If
            AddToInfo("Got Document Class List!")
        Catch ex As System.Web.Services.Protocols.SoapException
            DisplaySoapException(ex)
        End Try

    End Sub

    Private Sub GetFieldInfo(ByVal DocClassName As String)

        Try

            fi = DMS.GetFieldInfo(DocClassName)
            If (fi.Length() > 0) Then
                UpdateSearchParmsPanel()
                AddToInfo("Got Field Information for DocClass: " & lbDocClasses.SelectedItem.ToString)
            End If
        Catch ex As System.Web.Services.Protocols.SoapException
            DisplaySoapException(ex)

        End Try
    End Sub

    Private Sub DisplaySoapException(ByVal ex As System.Web.Services.Protocols.SoapException)
        Dim Info As String
        tbAppInfo.Items.Clear()
        AddToInfo("System.Web.Services.Protocols.SoapException caught!")
        AddToInfo("Message: " & ex.Message)
        AddToInfo("Detail.OuterXml:")
        AddToInfo(ex.Detail.OuterXml)

    End Sub

    Private Sub lbDocClasses_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbDocClasses.SelectedIndexChanged
        Dim DocClassName As String
        DocClassName = lbDocClasses.SelectedItem.ToString
        GetFieldInfo(DocClassName)
    End Sub

    Private Sub UpdateSearchParmsPanel()

        pnlSearchParms.Controls.Clear()
        ReDim Lbls(fi.Length)
        ReDim Values(fi.Length)
        Dim i As Integer
        Dim Top As Integer
        Dim LabelsLeft As Integer
        Dim ValueLeft As Integer
        Dim ValueWidth As Integer
        LabelsLeft = 5
        ValueWidth = 150
        ValueLeft = 125
        Top = 5

        For i = LBound(fi) To UBound(fi)
            Lbls(i) = New Label
            Lbls(i).AutoSize = True
            Lbls(i).Text = fi(i).FieldDesc + "(" + Trim(fi(i).FieldName) + ")"
            Lbls(i).Top = Top
            Lbls(i).Left = LabelsLeft
            ValueLeft = Lbls(i).Left + Lbls(i).Width + 10

            Values(i) = New TextBox
            Values(i).Top = Top
            Values(i).Left = ValueLeft
            Values(i).MaxLength = fi(i).FieldLen
            Values(i).Width = ValueWidth
            pnlSearchParms.Controls.Add(Lbls(i))
            pnlSearchParms.Controls.Add(Values(i))

            Top = Lbls(i).Top + Lbls(i).Height + 7

        Next

    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim Search As New DMSWEBAPI.SearchCriteria
        Search.DocClass = lbDocClasses.SelectedItem.ToString()
        Search.Values = fi
        Search.MaxHits = nudMaxHits.Value
        Search.CacheResults = cbCacheResults.Checked()

        ' Set the globals
        LastRec = 0
        CacheID = 0

        Dim i As Integer
        For i = LBound(fi) To UBound(fi)
            Search.Values(i).FieldValue = Values(i).Text
        Next

        sr = DMS.DoSearch(Search)
        If (sr.HitCount > 0) Then
            ParseAndDisplaySearchResults(sr)
        
            If (cbCacheResults.Checked) Then
                btnGetNextHits.Enabled = True
                CacheID = sr.CacheID
                LastRec = sr.HitCount

            Else
                btnGetNextHits.Enabled = False
            End If
            AddToInfo("Records returned: " & sr.HitCount.ToString & "(MaxHits = " & Search.MaxHits & ")")
        Else
            AddToInfo("No Records Returned")
        End If

    End Sub
    Private Sub ParseAndDisplaySearchResults(ByRef sr As DMSWEBAPI.SrchResults)

        Dim Item As DMSWEBAPI.SrchItem
        Dim Field As DMSWEBAPI.DMSField
        Dim i As Integer
        Dim s As String
        lbSearchResults.Items.Clear()
        SelectedSearchIndex = -1
        btnRetrieveFile.Enabled = False

        If sr.HitCount > 0 Then
            For Each Item In sr.Items
                s = ""
                s = s + "[DocID:" + Item.Meta.Batch + "-" + Item.Meta.SeqNo.ToString + "]"
                For Each Field In Item.Fields
                    s = s + "[" + Field.Name + ":" + Field.Value + "] "
                Next
                lbSearchResults.Items.Add(s)
            Next
        End If
        
        
    End Sub
    Private Sub btnUploadFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadFile.Click

        Dim Proxy As New DMSWEBAPI.DMSAPIWse
        Dim FileName As String
        Dim sw As New Stopwatch
        Dim t As Double
        Dim s As Long

        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            FileName = OpenFileDialog1.FileName

            Dim DFields(fi.Length - 1) As DMSWEBAPI.DMSField

            Dim infile As New IO.StreamReader(FileName)
            s = infile.BaseStream.Length()
            Dim attach As New DimeAttachment(Path.GetExtension(FileName), TypeFormat.MediaType, infile.BaseStream)

            Dim i As Integer
            For i = LBound(fi) To UBound(fi)
                Dim DF As New DMSWEBAPI.DMSField
                DF.Name = fi(i).FieldName
                DF.Value = Values(i).Text   ' Get the field values from the form
                DFields(i) = DF
            Next
            Dim BatchSeq As String
            Try
                Proxy.RequestSoapContext.Attachments.Add(attach)

                sw.Start()
                BatchSeq = Proxy.UploadFile(lbDocClasses.SelectedItem.ToString, DFields)
                t = sw.Done()
            Catch ex As System.Web.Services.Protocols.SoapException
                DisplaySoapException(ex)
                Exit Sub
            End Try
            tbAppInfo.Items.Clear()
            AddToInfo("Uploaded file: " & BatchSeq)
            AddToInfo("Time to upload file of " & Trim(Str(s)) & " bytes: " & Str(t) & " seconds.")

        End If



        
    End Sub

    Private Sub btnRetrieveFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetrieveFile.Click

        Dim Item As DMSWEBAPI.ItemMetaData
        Item = sr.Items(SelectedSearchIndex).Meta
        Dim FileName As String

        Dim sw As New Stopwatch
        sw.Start()
        Try
            FileName = DMS.RetrieveFile(Item)
        Catch ex As System.Web.Services.Protocols.SoapException
            DisplaySoapException(ex)
            Exit Sub
        End Try

        Dim t As Double
        t = sw.Done()

        AddToInfo("Time to retrieve the file: " & Str(t) & " seconds")
        Dim TempPath As String
        TempPath = Path.GetTempPath
        TempPath = TempPath + "\" + FileName

        If DMS.ResponseSoapContext.Attachments.Count() <> 1 Then
            Throw New ArgumentException("Please download one file at a time...")
        End If

        If File.Exists(TempPath) Then
            File.Delete(TempPath)
        End If

        sw.Start()

        Dim fs As FileStream = File.Create(TempPath)
        Dim fileLength As Integer = DMS.ResponseSoapContext.Attachments.Item(0).Stream.Length()
        Dim buffer(fileLength) As Byte
        DMS.ResponseSoapContext.Attachments.Item(0).Stream.Read(buffer, 0, fileLength)
        fs.Write(buffer, 0, fileLength)
        fs.Close()
        t = sw.Done()
        AddToInfo("Time to save the file (" & Str(fileLength) & " bytes) : " & Str(t) & " seconds")



        Dim psi As New ProcessStartInfo
        psi.UseShellExecute = True
        psi.FileName = TempPath
        Process.Start(psi)


    End Sub

    Private Sub lbSearchResults_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbSearchResults.SelectedIndexChanged
        SelectedSearchIndex = lbSearchResults.SelectedIndex
        btnRetrieveFile.Enabled = True
        Dim Msg As String

        Msg = "Selected Search Result Batch/Seq: " & sr.Items(SelectedSearchIndex).Meta.Batch & ":" & sr.Items(SelectedSearchIndex).Meta.SeqNo
        AddToInfo(Msg)

    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        tbAppInfo.Items.Clear()
    End Sub
    Private Sub AddToInfo(ByVal Msg As String)
        tbAppInfo.Items.Add(Msg)
    End Sub


    Private Sub ContextMenu1_Popup(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click

        Dim AllText As String
        Dim Line As String

        For Each Line In tbAppInfo.SelectedItems
            AllText = AllText + Line + Chr(13) + Chr(10)

        Next
        If (AllText.Length > 0) Then Clipboard.SetDataObject(AllText)

    End Sub

    Private Sub btnGetNextHits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetNextHits.Click
    
        If (CacheID > 0) Then
            sr = DMS.GetCachedHits(CacheID, LastRec, nudMaxHits.Value)
            LastRec = LastRec + sr.HitCount

            ParseAndDisplaySearchResults(sr)


            AddToInfo("Records returned from Cache: " & sr.HitCount.ToString & "(Total Hits = " & sr.TotalHits & ")")
        Else
            AddToInfo("No Cached Hits to Get")
        End If
    End Sub

    Private Sub btnUploadString_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadString.Click

        Dim Proxy As New DMSWEBAPI.DMSAPIWse
        Dim sw As New Stopwatch
        Dim t As Double
        Dim s As Long

        If (Len(txtUploadString.Text) > 0) Then

            Dim DFields(fi.Length - 1) As DMSWEBAPI.DMSField

            Dim i As Integer
            For i = LBound(fi) To UBound(fi)
                Dim DF As New DMSWEBAPI.DMSField
                DF.Name = fi(i).FieldName
                DF.Value = Values(i).Text   ' Get the field values from the form
                DFields(i) = DF
            Next

            Dim BatchSeq As String
            Try
                sw.Start()
                BatchSeq = Proxy.UploadString(lbDocClasses.SelectedItem.ToString, DFields, txtUploadString.Text, "txt")
                t = sw.Done()
            Catch ex As System.Web.Services.Protocols.SoapException
                DisplaySoapException(ex)
                Exit Sub
            End Try

            tbAppInfo.Items.Clear()
            AddToInfo("Uploaded string: " & BatchSeq)
            AddToInfo("Time to upload string: " & Str(t) & " seconds.")

        End If
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim DocClassName As String

        tbAppInfo.Items.Clear()
        Try

            'DocClassName = DMS.GetDocClassName(tbDocID.Text)
            'AddToInfo("DocClass Name for DocID: " + tbDocID.Text + " = [" + DocClassName + "]")

        Catch ex As Exception

            TraceExceptions("Button2_Click", ex)

        End Try
        
    End Sub
    Sub TraceExceptions(ByVal ProcName As String, ByVal ex As Exception)

        Dim NewMsg As String
        NewMsg = ProcName + ": The following exception occurred: " + ex.ToString()
        AddToInfo(NewMsg)
        'check the InnerException
        While Not (ex.InnerException Is Nothing)
            AddToInfo(ProcName + ": --------------------------------")
            AddToInfo("DMSWebAPI: " + ProcName + ":  Next: " + ex.InnerException.ToString())
            ex = ex.InnerException
        End While

    End Sub
End Class
