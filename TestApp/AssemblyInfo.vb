Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("DMSWebAPI Test Application")> 
<Assembly: AssemblyDescription("Test/Demo Application for the VIPDMS Web based API")> 
<Assembly: AssemblyCompany("Open Text Corporation")> 
<Assembly: AssemblyProduct("DMSWebAPITest")> 
<Assembly: AssemblyCopyright("Copyright � 2005 Open Text Corporation")> 
<Assembly: AssemblyTrademark("All Rights Reserved")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("1D31340F-0C1C-4845-AEA1-C1D558DAC7AE")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("8.5.*")> 
