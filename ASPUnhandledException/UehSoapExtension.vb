Imports System.IO
Imports System.Web.Services
Imports System.Web.Services.Protocols

''' <summary>
''' Implements ASP unhandled exception manager as a SoapExtension
''' </summary>
''' <remarks>
''' to use:
''' 
'''    1) place ASPUnhandledException.dll in the \bin folder
'''    2) add this section to your Web.config under the &lt;webServices&gt; element:
'''			&lt;!-- Adds our error handler to the SOAP pipeline. --&gt;
'''		    &lt;soapExtensionTypes&gt;
'''				&lt;add type="ASPUnhandledException.UehSoapExtension, ASPUnhandledException"
'''				     priority="1" group="0" /&gt;
'''			&lt;/soapExtensionTypes&gt;
'''
'''  Jeff Atwood
'''  http://www.codinghorror.com/
''' </remarks>
Public Class UehSoapExtension
    Inherits SoapExtension

    Private _OldStream As Stream
    Private _NewStream As Stream

    Public Overloads Overrides Function GetInitializer(ByVal serviceType As System.Type) As Object
        Return Nothing
    End Function

    Public Overloads Overrides Function GetInitializer(ByVal methodInfo As System.Web.Services.Protocols.LogicalMethodInfo, ByVal attribute As System.Web.Services.Protocols.SoapExtensionAttribute) As Object
        Return Nothing
    End Function

    Public Overrides Sub Initialize(ByVal initializer As Object)
    End Sub

    Public Overrides Function ChainStream(ByVal stream As Stream) As Stream
        _OldStream = stream
        _NewStream = New MemoryStream
        Return _NewStream
    End Function

    Private Sub Copy(ByVal fromStream As Stream, ByVal toStream As Stream)
        Dim sr As New StreamReader(fromStream)
        Dim sw As New StreamWriter(toStream)
        sw.Write(sr.ReadToEnd())
        sw.Flush()
    End Sub

    Public Overrides Sub ProcessMessage(ByVal message As System.Web.Services.Protocols.SoapMessage)

        Select Case message.Stage
            Case SoapMessageStage.BeforeDeserialize
                Copy(_OldStream, _NewStream)
                _NewStream.Position = 0

            Case SoapMessageStage.AfterSerialize
                If Not message.Exception Is Nothing Then
                    Dim ueh As New Handler
                    Dim strDetailNode As String
                    '-- handle our exception, and get the SOAP <detail> string
                    strDetailNode = ueh.HandleWebServiceException(message)
                    '-- read the entire SOAP message stream into a string
                    _NewStream.Position = 0
                    Dim tr As TextReader = New StreamReader(_NewStream)
                    '-- insert our exception details into the string
                    Dim s As String = tr.ReadToEnd
                    s = s.Replace("<detail />", strDetailNode)
                    '-- overwrite the stream with our modified string
                    _NewStream = New MemoryStream
                    Dim tw As TextWriter = New StreamWriter(_NewStream)
                    tw.Write(s)
                    tw.Flush()
                End If
                _NewStream.Position = 0
                Copy(_NewStream, _OldStream)

        End Select

    End Sub

End Class
