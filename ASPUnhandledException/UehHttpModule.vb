Imports System.Web

''' <summary>
''' Implements ASP unhandled exception manager as a HttpModule
''' </summary>
''' <remarks>
''' to use:
'''    1) place ASPUnhandledException.dll in the \bin folder
'''    2) add this section to your Web.config under the &lt;system.web&gt; element:
'''         &lt;httpModules&gt;
'''	 	        &lt;add name="ASPUnhandledException" 
'''                 type="ASPUnhandledException.UehHttpModule, ASPUnhandledException" /&gt;
'''		    &lt;/httpModules&gt;
'''
'''  Jeff Atwood
'''  http://www.codinghorror.com/
''' </remarks>
Public Class UehHttpModule
    Implements IHttpModule

    Public Sub Init(ByVal Application As System.Web.HttpApplication) Implements System.Web.IHttpModule.Init
        AddHandler Application.Error, AddressOf OnError
    End Sub

    Public Sub Dispose() Implements System.Web.IHttpModule.Dispose
    End Sub

    Protected Overridable Sub OnError(ByVal sender As Object, ByVal args As EventArgs)
        Dim app As HttpApplication = CType(sender, HttpApplication)
        Dim ueh As New Handler
        ueh.HandleException(app.Server.GetLastError)
    End Sub
End Class
