What is it?

This HTTP module implements global Unhandled Exception notification for any ASP.NET website, 
or any ASP.NET Web Service, without recompilation. When your application encounters an Unhandled 
Exception, you get the following for free:

 - a "user friendly" exception web page
 - an email notification 
 - a plain text logfile 
 - Event Log logging
 
How do I set it up?

This is the Quick Start. Refer to source headers for more options and details.

1) put ASPUnhandledException.dll in the /bin folder

2) Add these configuration options to the <appSettings> section of your Web.config

	<configSections>    
		<section name="UnhandledException" type="System.Configuration.NameValueSectionHandler, 
			System, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
	</configSections>

	<UnhandledException>
  		<add key="EmailTo" value="me@domain.com;you@domain.com" />	
		<add key="ContactInfo" value="Ima TestGuy at 919-555-1212" />
		<add key="IgnoreRegex" value="get_aspx_ver\.aspx" />
		<add key="SmtpDefaultDomain" value="mydomain.com" />
		<add key="SmtpServer" value="mail.mydomain.com" />
	</UnhandledException>

3A) for an ASP.NET website, modify the <system.web> section of your Web.config

  <system.web>
	<!-- Adds our error handler to the HTTP pipeline -->
	<httpModules>
	  <add name="UehHttpModule" 
	       type="ASPUnhandledException.UehHttpModule, ASPUnhandledException" />
	</httpModules>
  </system.web>
  
2B) for an ASP.NET Web Service, modify the <webServices> section of your Web.config:

  <webServices>
	<!-- Adds our error handler to the SOAP pipeline -->
	<soapExtensionTypes>
	  <add type="ASPUnhandledException.UehSoapExtension, ASPUnhandledException"
	       priority="1" group="0" />
	</soapExtensionTypes>
  </webServices>