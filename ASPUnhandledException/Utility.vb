Module Utility

    Sub TraceExceptions(ByVal ProcName As String, ByVal ex As Exception)

        Dim NewMsg As String
        NewMsg = ProcName + ": The following exception occurred: " + ex.ToString()
        Trace.WriteLine("DMSWebAPI:" + NewMsg)
        'check the InnerException
        While Not (ex.InnerException Is Nothing)
            Trace.WriteLine("DMSWebAPI: " + ProcName + ": --------------------------------")
            Trace.WriteLine("DMSWebAPI: " + ProcName + ":  Next: " + ex.InnerException.ToString())
            ex = ex.InnerException
        End While

    End Sub

    Sub DbgMsg(ByVal Msg As String)

        Dim NewMsg As String
        NewMsg = "ASPUnhandledException: " & Msg
        Trace.Write(NewMsg)
    End Sub


End Module
