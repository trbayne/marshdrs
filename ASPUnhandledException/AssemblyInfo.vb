Imports System
Imports System.Reflection

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ASPUnhandledException")> 
<Assembly: AssemblyDescription("ASP.NET unhandled exception handling library")> 
<Assembly: AssemblyCompany("Atwood Heavy Industries")> 
<Assembly: AssemblyProduct("Exception Handling Framework")> 
<Assembly: AssemblyCopyright("� 2004, Atwood Heavy Industries")> 
<Assembly: AssemblyTrademark("All Rights Reserved")> 
<Assembly: CLSCompliant(True)> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("2.1.*")> 
