Imports System.Configuration
Imports System.Collections.Specialized
Imports System.IO

'--
'-- processes custom .config section as follows:
'--
'-- <configSections>    
'--	    <section name="UnhandledException" type="System.Configuration.NameValueSectionHandler, System, 
'--        Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
'-- </configSections>
'--
'-- <!-- settings for UnhandledExceptionManager -->
'--	<UnhandledException>
'--		<add key="ContactInfo" value="Ima Testguy at 123-555-1212" />
'--		<add key="IgnoreDebug" value="False" />
'--		<add key="IgnoreRegex" value="get_aspx_ver\.aspx" />
'--		<add key="EmailTo" value="me@mydomain.com" />
'--	</UnhandledException>   
'--
'-- Complete configuration options are..
'--  
'--  <!-- Handler defaults -->
'--  <add key="AppName" value="" />
'--  <add key="ContactInfo" value="" />
'--  <add key="EmailTo" value="" />
'--  <add key="LogToEventLog" value="False" />
'--  <add key="LogToFile" value="True" />
'--  <add key="LogToEmail" value="True" />
'--  <add key="LogToUI" value="True" />
'--	 <add key="PathLogFile" value="" />
'--  <add key="IgnoreRegex" value="" />
'--  <add key="IgnoreDebug" value="True" />
'--  <add key="WhatHappened" value="There was an unexpected error in this website. This may be due to a programming bug." />
'--  <add key="HowUserAffected" value="The current page will not load." />
'--  <add key="WhatUserCanDo" value="Close your browser, navigate back to this website, and try repeating your last action. Try alternative methods of performing the same action. If problems persist, contact (contact)." />
'--
'--  <!-- SMTP email configuration defaults -->
'--  <add key="SmtpDefaultDomain" value="mydomain.com" />
'--  <add key="SmtpServer" value="mail.mydomain.com" />
'--  <add key="SmtpPort" value="25" />
'--  <add key="SmtpAuthUser" value="" />
'--  <add key="SmtpAuthPassword" value="" />
'--

''' <summary>
''' Minimal class for retrieving typed values from the 
''' &lt;UnhandledException&gt; custom NameValueCollection .config section
''' </summary>
Friend Class Config

    Private Const _strSectionName As String = "UnhandledException"
    Private Shared _nvc As NameValueCollection

    Private Shared Sub Load()
        If Not _nvc Is Nothing Then Return

        Dim o As Object
        Try
            o = ConfigurationSettings.GetConfig(_strSectionName)
        Catch ex As Exception
            '-- we are in an unhandled exception handler
        End Try
        If o Is Nothing Then
            '-- we can work without any configuration at all (all defaults)
            _nvc = New NameValueCollection
            Return
        End If

        Try
            _nvc = CType(o, NameValueCollection)
        Catch ex As Exception
            Throw New ConfigurationException("The <" & _strSectionName & "> section is present in the .config file, but it does not appear to be a name value collection.", ex)
        End Try
    End Sub

    ''' <summary>
    ''' retrieves integer value from the 
    ''' &lt;UnhandledException&gt; custom NameValueCollection .config section
    ''' </summary>
    Public Shared Function GetInteger(ByVal Key As String, _
        ByVal [Default] As Integer) As Integer

        Load()
        Dim strTemp As String = _nvc.Item(Key)
        If strTemp = "" Then
            Return [Default]
        End If

        Try
            Return Convert.ToInt32(strTemp)
        Catch ex As Exception
            Return [Default]
        End Try
    End Function

    ''' <summary>
    ''' retrieves boolean value from the 
    ''' &lt;UnhandledException&gt; custom NameValueCollection .config section
    ''' </summary>
    Public Shared Function GetBoolean(ByVal Key As String, _
        Optional ByVal [Default] As Boolean = Nothing) As Boolean

        Load()
        Dim strTemp As String = _nvc.Item(Key)
        If strTemp = "" Then
            Return [Default]
        End If

        Select Case strTemp.ToLower
            Case "1", "true"
                Return True
            Case Else
                Return False
        End Select
    End Function

    ''' <summary>
    ''' retrieves string value from the 
    ''' &lt;UnhandledException&gt; custom NameValueCollection .config section
    ''' </summary>
    Public Shared Function GetString(ByVal Key As String, _
        Optional ByVal [Default] As String = Nothing) As String

        Load()
        Dim strTemp As String = _nvc.Item(Key)
        If strTemp = "" Then
            Return [Default]
        End If

        Return strTemp
    End Function

    ''' <summary>
    ''' retrieves relative or absolute path string from the 
    ''' &lt;UnhandledException&gt; custom NameValueCollection .config section
    ''' </summary>
    Public Shared Function GetPath(ByVal Key As String) As String

        Load()
        Dim strPath As String = GetString(Key, "")

        '-- users might think we're using Server.MapPath, but we're not
        '-- strip this because it's unnecessary (we assume website root, if path isn't rooted)
        If strPath.StartsWith("~/") Then
            strPath = strPath.Replace("~/", "")
        End If
        If Path.IsPathRooted(strPath) Then
            Return strPath
        Else
            Return Path.Combine(AppBase, strPath)
        End If
    End Function

    Private Shared ReadOnly Property AppBase() As String
        Get
            Return Convert.ToString(System.AppDomain.CurrentDomain.GetData("APPBASE"))
        End Get
    End Property

End Class